<?php

add_theme_support( 'post-thumbnails' );

# Disable gutenberg Widgets fullkade.com
add_filter( 'gutenberg_use_widgets_block_editor', '__return_false' );
add_filter( 'use_widgets_block_editor', '__return_false' );

// Load in our CSS
function wptags_enqueue_styles() {

  wp_enqueue_style( 'main-css', get_stylesheet_directory_uri() . '/style.css');
  wp_enqueue_style( 'custom-css', get_stylesheet_directory_uri() . '/assets/css/custom.css');
  wp_enqueue_style( 'header-css', get_stylesheet_directory_uri() . '/assets/css/headers.css');
  wp_enqueue_style( 'blog-css', get_stylesheet_directory_uri() . '/assets/css/blog.css');
  wp_enqueue_style( 'blog-rtl-css', get_stylesheet_directory_uri() . '/assets/css/blog-rtl.css');
  wp_enqueue_style( 'style1', 'https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css');
  //wp_enqueue_style( 'style3', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css');
  wp_enqueue_style( 'style2', 'https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css');
  wp_enqueue_style( 'font-awsome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css');
  wp_enqueue_style( 'costum_design', get_stylesheet_directory_uri() . '/costum_design.css');


  wp_enqueue_style( 'author-css', get_stylesheet_directory_uri() . '/assets/css/author.css');
  wp_enqueue_style( 'category-css', get_stylesheet_directory_uri() . '/assets/css/category.css');
  wp_enqueue_style( 'tag-css', get_stylesheet_directory_uri() . '/assets/css/tag.css');
  wp_enqueue_style( 'search-css', get_stylesheet_directory_uri() . '/assets/css/search.css');
  wp_enqueue_style( 'single-post-css', get_stylesheet_directory_uri() . '/assets/css/single-post.css');
  wp_enqueue_style( 'page-user-css', get_stylesheet_directory_uri() . '/assets/css/page-user.css');



//wp_enqueue_style( 'blog-css2', get_stylesheet_directory_uri() . '/css/style.css');
  



}
add_action( 'wp_enqueue_scripts', 'wptags_enqueue_styles' );

// Load in our JS
function wptags_enqueue_scripts() {
	wp_enqueue_script( 'jquery-theme-jsbs1','https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js');

  // wp_enqueue_script( 'theme-js', get_stylesheet_directory_uri() . '/assets/js/theme.js', [], time(), true );
  //wp_enqueue_script( 'jquery-theme-js', get_stylesheet_directory_uri() . '/assets/js/jquery.theme.js', [ 'jquery' ], time(), true );
  wp_enqueue_script( 'jquery-theme-jsbs','https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js');


}
add_action( 'wp_enqueue_scripts', 'wptags_enqueue_scripts' );


function main_theme_setup() {


	/*
		* Let WordPress manage the document title.
		* By adding theme support, we declare that this theme does not use a
		* hard-coded <title> tag in the document head, and expect WordPress to
		* provide it for us.
		*/

	/*
		* Enable support for Post Thumbnails on posts and pages.
		*
		* @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		*/
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus(
		array(
			'menu_1_admin' => 'menu_1_admin',
			'menu_2_admin' => 'menu_2_admin',
		)
	);

	/*
		* Switch default core markup for search form, comment form, and comments
		* to output valid HTML5.
		*/
	add_theme_support(
		'html5',
		array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
			'style',
			'script',
		)
	);

	// Set up the WordPress core custom background feature.
	add_theme_support(
		'custom-background',
		apply_filters(
			'underscores_theme_1_custom_background_args',
			array(
				'default-color' => 'ffffff',
				'default-image' => '',
			)
		)
	);

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );

	/**
	 * Add support for core custom logo.
	 *
	 * @link https://codex.wordpress.org/Theme_Logo
	 */
	add_theme_support(
		'custom-logo',
		array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		)
	);
 }
add_action( 'after_setup_theme', 'main_theme_setup' );








//add classes to <a> in menu items

function add_menu_link_class( $atts, $item, $args ) {
	if (property_exists($args, 'link_class')) {
	  $atts['class'] = $args->link_class;
	}
	return $atts;
  }
add_filter( 'nav_menu_link_attributes', 'add_menu_link_class', 1, 3 );








// set logout button
function logout(){
    if(isset($_POST['button1']) && is_user_logged_in()){
		
		wp_redirect( wp_logout_url());
		exit;
		

	  }
	  if(isset($_POST['button2']) && is_user_logged_in()){
		unset($_POST['button2']);
        wp_logout();
		wp_safe_redirect( get_permalink() );
	
		

	  }
	  if(isset($_POST['button4']) && is_user_logged_in()){
		
		wp_redirect( 'http://localhost/custom_theme_001/log/');
		//wp_safe_redirect( 'mylogout.php' );

       // get_template_part( 'mylogout.php' );
		exit;
		

	  }
     }
add_action('init', 'logout');








// Setup Widget Areas
function wptags_widgets_init() {
	register_sidebar([
	  'name'          =>'Main Sidebar',
	  'id'            => 'main-sidebar',
	  'description'   =>'Add widgets for main sidebar here',
	  'before_widget' => '<section class="widget">',
	  'after_widget'  => '</section>',
	  'before_title'  => '<h2 class="widget-title">',
	  'after_title'   => '</h2>',
	]);
  }
  add_action( 'widgets_init', 'wptags_widgets_init' );









/*
// redirect non-logged in users to home page!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1

function redirect_non_logged_users_to_specific_page() {

if ( !is_user_logged_in() && is_page('page-user') && $_SERVER['PHP_SELF'] != '/wp-admin/admin-ajax.php' ) {

wp_redirect( 'https://wphierarchy.com/' ); 
    exit;
   }
}
//add_action( 'init', 'redirect_non_logged_users_to_specific_page' );

*/

//redirect non-logged in users to home page correct!

add_action( 'template_redirect', 'redirect_to_specific_page' );

function redirect_to_specific_page() {

if ( is_page('user') && ! is_user_logged_in() ) {

wp_redirect( wp_login_url(home_url().'/user/' ) , 301 ); 
  exit;
    }
}





// custom previous/next_post_link function
function next_link(){
	$next_post_obj  = get_adjacent_post( '', '', false );
	$next_post_ID   = isset( $next_post_obj->ID ) ? $next_post_obj->ID : '';
	$next_post_link     = get_permalink( $next_post_ID );
	return $next_post_link;

}function previous_link(){
	$previous_post_obj  = get_adjacent_post( '', '', true );
	$previous_post_ID   = isset( $previous_post_obj->ID ) ? $previous_post_obj->ID : '';
	$previous_post_link     = get_permalink( $previous_post_ID );
	return $previous_post_link;

}
add_action('init', 'next_link');
add_action('init', 'previous_link');







// custom previous/next_posts_link function in blog page
function my_get_next_posts_link( $label = null, $max_page = 0 ) {
    global $paged, $wp_query;
 
    if ( ! $max_page ) {
        $max_page = $wp_query->max_num_pages;
    }
 
    if ( ! $paged ) {
        $paged = 1;
    }
 
    $nextpage = (int) $paged + 1;
 
    if ( null === $label ) {
        $label = __( 'Next Page &raquo;' );
    }
 
    if ( ! is_single() && ( $nextpage <= $max_page ) ) {
        /**
         * Filters the anchor tag attributes for the next posts page link.
         *
         * @since 2.7.0
         *
         * @param string $attributes Attributes for the anchor tag.
         */
        $attr = apply_filters( 'next_posts_link_attributes', '' );
 
        return  next_posts( $max_page, false );
    }
}

function my_get_previous_posts_link( $label = null ) {
    global $paged;
 
    if ( null === $label ) {
        $label = __( '&laquo; Previous Page' );
    }
 
    if ( ! is_single() && $paged > 1 ) {
        /**
         * Filters the anchor tag attributes for the previous posts page link.
         *
         * @since 2.7.0
         *
         * @param string $attributes Attributes for the anchor tag.
         */
        $attr = apply_filters( 'previous_posts_link_attributes', '' );
        return  previous_posts( false ) ;
    }
}

//add_action('init','my_get_next_posts_link');
//add_action('init','my_get_previous_posts_link');


// code for Add Numeric Pagination
function wpbeginner_numeric_posts_nav() {
 
    if( is_singular() )
        return;
 
    global $wp_query;
 
    /** Stop execution if there's only 1 page */
    if( $wp_query->max_num_pages <= 1 )
        return;
 
    $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
    $max   = intval( $wp_query->max_num_pages );
 
    /** Add current page to the array */
    if ( $paged >= 1 )
        $links[] = $paged;
 
    /** Add the pages around the current page to the array */
    if ( $paged >= 3 ) {
        $links[] = $paged - 1;
        $links[] = $paged - 2;
    }
 
    if ( ( $paged + 2 ) <= $max ) {
        $links[] = $paged + 2;
        $links[] = $paged + 1;
    }
 
    echo '<div class="navigation"><ul>' . "\n";
 
    /** Previous Post Link */
    if ( get_previous_posts_link() )
        printf( '<li>%s</li>' . "\n", get_previous_posts_link() );
 
    /** Link to first page, plus ellipses if necessary */
    if ( ! in_array( 1, $links ) ) {
        $class = 1 == $paged ? ' class="active"' : '';
 
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );
 
        if ( ! in_array( 2, $links ) )
            echo '<li>…</li>';
    }
 
    /** Link to current page, plus 2 pages in either direction if necessary */
    sort( $links );
    foreach ( (array) $links as $link ) {
        $class = $paged == $link ? ' class="active"' : '';
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
    }
 
    /** Link to last page, plus ellipses if necessary */
    if ( ! in_array( $max, $links ) ) {
        if ( ! in_array( $max - 1, $links ) )
            echo '<li>…</li>' . "\n";
 
        $class = $paged == $max ? ' class="active"' : '';
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
    }
 
    /** Next Post Link */
    if ( get_next_posts_link() )
        printf( '<li>%s</li>' . "\n", get_next_posts_link() );
 
    echo '</ul></div>' . "\n";
 
}


// show custom thumbnail
function my_thumbnail(){
    if( has_post_thumbnail()) {
        echo get_the_post_thumbnail_url();
      
    }else{
        echo 'https://via.placeholder.com/400x150/6495ED/000000';
    }
}






/**
 * Display time since post was published
 *
 * @uses	human_time_diff()  Return time difference in easy to read format
 * @uses	get_the_time()  Get the time the post was published
 * @uses	current_time()  Get the current time
 *
 * @return	string  Timestamp since post was published
 *
 * @author c.bavota
 */
function get_time_since_posted() {

	$time_since_posted = human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) . ' پیش';

	return $time_since_posted;

}


















?>