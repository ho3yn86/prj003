<?php get_header();?>


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/all.min.css" integrity="sha256-2XFplPlrFClt0bIdPgpz8H7ojnk10H69xRqd9+uTShA=" crossorigin="anonymous" />

<section>
<div class="container">
    <div class="text-center mb-5">
        <h5 class="text-primary h6"><?php single_tag_title(); ?></h5>
        <h2 class="display-20 display-md-18 display-lg-16"><?php single_tag_title(); ?></h2>
    </div>
    <div class="row">
        <?php  if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <div class="col-lg-4 col-md-6 mb-2-6">
            <article class="card card-style2">
                <div class="card-img">
                    <img id="my_thumbnail" class="rounded-top" src="<?php my_thumbnail()?>" alt="...">
                    <div class="date"><span><?php echo get_the_date('d'); ?></span><?php echo get_the_date('F'); ?></div>
                </div>
                <div class="card-body">
                    <h3 class="h5"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
                    <p class="display-30"><?php echo wp_trim_words(get_the_excerpt(), 15); ?></p>
                    <a href="<?php the_permalink(); ?>" class="read-more">بیشتر بخوانید ... </a>
                </div>
                <div class="card-footer">
                    <ul>
                        <li><a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) );?>"><i class="fas fa-user"></i><?php the_author(); ?></a></li>
                        <li><a href="#!"><i class="far fa-comment-dots"></i><span><?php comments_number();?></span></a></li>
                    </ul>
                </div>
            </article>
        </div>
        <?php endwhile;endif;?>
    </div>
</div>
</section>




<?php get_footer(); ?>
