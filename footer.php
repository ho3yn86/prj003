
</main>
<?php wp_footer(); ?>

            <div class="b-example-divider"></div>

            <div class="container">
            <footer class="row row-cols-5 py-5 my-5 border-top">
                <div class="col">
                <a href="/" class="d-flex align-items-center mb-3 link-dark text-decoration-none">
                    <svg class="bi me-2" width="40" height="32"><use xlink:href="#bootstrap"/></svg>
                </a>
                <p class="text-muted">&copy; 2021</p>
                </div>

                <div class="col">

                </div>

                <div class="col">
                <h5>پر بازدید ها</h5>
                <ul class="nav flex-column">
                    <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted">Home</a></li>
                    <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted">Features</a></li>
                    <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted">Pricing</a></li>
                    <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted">FAQs</a></li>
                    <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted">About</a></li>
                </ul>
                </div>

                <div class="col">
                <h5>جدید ترین مطالب</h5>
                <ul class="nav flex-column">
                                <?php
                $defaults1 = array(
                    'numberposts'      => 6,
                    'category'         => 0,
                    'orderby'          => 'date',
                    'order'            => 'DESC',
                );
                $post1=get_posts($defaults1);
                foreach($post1 as $mypost) 
                  { ?>
                <li class="nav-item mb-2"><a href="<?php echo get_permalink($mypost->post_id); ?>" class="nav-link p-0 text-muted"><?php echo $mypost->post_title ?></a></li><?php
                    }?>
                </ul>
                </div>

                <div class="col">
                <h5>: دسته بندی ها</h5>
                <ul class="nav flex-column">
                     <?php
                        $categories=get_categories($cat_args);
                        foreach($categories as $category) { ?>
                        <li class="nav-item mb-2"><a href="<?php echo get_category_link( $category->term_id );?>" class="nav-link p-0 text-muted"><?php echo $category->name; ?></a></li>
                        <?php
                        }
                        ?>
                </ul>
              </div>
            </footer>
            </div>
</footer>
</body>

</div>

</html>



