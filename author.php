<?php get_header(); ?>
<div class="container bootstrap snippets bootdey">
    <div class="profile card">
        <div class="profile-body">
            <div class="profile-bio">
                <div class="row">
                    <div class="col-md-5 text-center">
                        <img class="img-thumbnail md-margin-bottom-10" src="<?php echo get_field('user_image', 'user_'.$current_user->ID) ?>" alt="">
                    </div>
                    <div class="col-md-7">
                        <h2><?php echo get_the_author() ?></h2>
                        <span><strong>تحصیلات :</strong> <?php echo get_metadata( 'user', $current_user->ID,'education',true )?></span>
                        <span><strong>تخصص :</strong> <?php echo get_metadata( 'user', $current_user->ID,'user_specialty',true )?></span>
                        <hr>
                        <p><?php echo get_the_author_meta('description'); ?></p>
                    </div>
                </div>    
            </div>
    	</div>
    </div>
</div>   


<div class="container blog-page">
        <div class="row clearfix">

        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
          <div  class="col-lg-4 col-md-12 my_post_id-<?php the_ID(); ?> ">
                <div class="card single_post">
                    <div class="header">
                    <h2> روتیتر <?php the_title(); ?></h2>

                    </div>
                    <div class="body">
                        <h3 class="m-t-0 m-b-5"><a href="<?php echo the_permalink(); the_permalink(); ?>"><h2><?php the_title(); ?></h2></a></h3>
                        <ul class="meta">
                            <li><a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) );?>" ><i class="zmdi zmdi-account col-blue"></i><?php the_author(); ?></a></li>
                            <li><a href="javascript:void(0);"><i class="zmdi zmdi-label col-amber"></i>	<?php the_tags( 'Tags: ', ', ', '<br />' ); ?></a></li>
                            <li><a href="javascript:void(0);"><i class="zmdi zmdi-comment-text col-blue"></i>Comments: 3</a></li>
                        </ul>
                    </div>
                    <div class="body">
                        <div class="img-post m-b-15">
                            <img src="<?php echo get_the_post_thumbnail_url($post->ID, 'full'); ?>" alt="Awesome Image">
                            <div class="social_share">
                                <button class="btn btn-primary btn-icon btn-icon-mini btn-round"><i class="zmdi zmdi-facebook"></i></button>
                                <button class="btn btn-primary btn-icon btn-icon-mini btn-round"><i class="zmdi zmdi-twitter"></i></button>
                                <button class="btn btn-primary btn-icon btn-icon-mini btn-round"><i class="zmdi zmdi-instagram"></i></button>
                            </div>
                        </div>
                        <p><?php the_excerpt(); ?></p>
                        <a href="<?php the_permalink(); ?>" title="read more" class="btn btn-round btn-info">Read More</a>
                    </div>
                </div>
            </div>
            <?php
            
            
            ?>
            <?php endwhile; endif; ?>
            <?php echo paginate_links(); ?>

        </div>
    </div>    


    <?php get_footer(); ?>