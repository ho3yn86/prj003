<?php get_header(); ?>

<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
<div class="container">
<section class="bg-light py-5">
        <div class="container">
          <div class="row">
            <div class="col-12">
              <h2 class="display-5 font-weight-bold mb-5"><?php single_tag_title(); ?></h2>
            </div>
          </div>
          <div class="row">
            <?php  if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
            <div class="col-md-6 col-xl-4">
              <div class="image-box image-box--shadowed white-bg style-2 mb-4">
                <div class="overlay-container text-center">
                  <img class="rounded" src="<?php my_thumbnail()?>" alt="" id="my_thumbnail">
                  <a href="<?php the_permalink() ?>" class="overlay-link"></a>
                </div>
                <div class="body">
                  <h5 class="font-weight-bold my-2"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h5>
                  <p class="small"><?php echo get_the_date() ?></p>
                  <div class=" align-items-center">
                    <div class="col-6">
                      <ul class="social-links  ">
                        <?php
                          $posttags = get_the_tags();
                          if ($posttags) {
                            foreach($posttags as $tag) {
                              //echo $tag->name . ' '; 
                              //var_dump($tag);
                              echo '<li class="text-secondary"><a href="'.home_url().'/tag/'.$tag->slug.'" class="text-muted">'.$tag->name.'</a></li>';
                            }
                          }
                          ?>
                      </ul>
                      
                    </div>
                    <div class="col-6 text-right">
                      <a href="<?php the_permalink() ?>" class="btn radius-50 btn-gray-transparent btn-animated">ادامه مطلب <i class="fa fa-arrow-right"></i></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <?php endwhile;endif;?>
          </div>
        </div>
      </section>
</div>

<div class="wpbeginner_numeric_posts_nav" >
<?php wpbeginner_numeric_posts_nav(); ?>
</div>



<?php get_footer(); ?>