<?php get_header(); ?>


<div class="container">
	<div class="row">



    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		<div class="row"> 
                    <div class="col-xs-12 col-sm-3 col-md-3">
                        <a href="<?php the_permalink();?>">
                            <img src="<?php my_thumbnail()?>" width="268px" height="198px" class="img-responsive img-box img-thumbnail"> 
                        </a>
                    </div> 
                    <div class="col-xs-12 col-sm-9 col-md-9">
                        <span><?php echo get_time_since_posted();?></span>
                        <h4><a href="<?php echo get_permalink() ?>"><?php the_title(); ?></a></h4>
                        <p><?php echo wp_trim_words(get_the_content(), 65); ?></p>
                    </div> 
                </div>
                <hr>
                <?php endwhile;endif; ?>
	</div>


    
    <div class="wpbeginner_numeric_posts_nav" >
    <?php wpbeginner_numeric_posts_nav(); ?>
    </div>

</div>



<?php get_footer(); ?>





