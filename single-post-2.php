<?php /* Template Name: قالب شماره 2 */ ?>

<?php get_header();?>


<div class="container">
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <div class="container pb50">
        <div class="row">
            <div class="col-md-9 mb40">
                <article>
                    <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="" class="img-fluid mb30">
                    <div class="post-content">
                        <h3><?php the_title() ?></h3>
                        <ul class="post-meta list-inline">
                            <li class="list-inline-item">
                                <i class="fa fa-user"></i> <a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) );?>"><?php the_author(); ?></a>
                            </li>
                            <li class="list-inline-item">
                                <i class="fa fa-calendar-o"></i> <a href="#">29 June 2017</a>
                            </li>
                            <li class="list-inline-item">
                                <i class="fa fa-tags"></i> <a href="#">Bootstrap4</a>
                            </li>
                            <li class="list-inline-item">
                                <p class="blog-post-meta"><?php  edit_post_link(); ?></p>
                            </li>
                        </ul>
                        <p> <?php the_content()?> </p>

                        <hr class="mb40">
                        <nav class="blog-pagination" aria-label="Pagination">
                        <a class="btn btn-outline-primary" href="<?php echo next_link() ?>">بعدی</a>
                        <a class="btn btn-outline-secondary " href="<?php echo previous_link() ?>" >قبلی</a>
                        </nav>

                        
                    </div>
                </article>
                <?php endwhile;
                
                endif;?>

            </div>
            <?php get_sidebar();?>
        </div>

    
    
    </div>
</div >

<?php get_footer(); ?>



