<?php get_header(); ?>
<div class="container">
    <div class="main-body">
    
          <!-- Breadcrumb -->
          <nav aria-label="breadcrumb" class="main-breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="index.html">Home</a></li>
              <li class="breadcrumb-item"><a href="javascript:void(0)">User</a></li>
              <li class="breadcrumb-item active" aria-current="page">User Profile</li>
            </ol>
          </nav>
          <!-- /Breadcrumb -->
    
          <div class="row gutters-sm">
            <div class="col-md-4 mb-3">
              <div class="card" id="profile-box-1">
                <div class="card-body">
                  <div class="d-flex flex-column align-items-center text-center">
                    <img src="<?php echo get_field('user_image', 'user_'.$current_user->ID) ?>" alt="Admin" class="rounded-circle" width="150">
                    <div class="mt-3">
                      <h4> <?php echo wp_get_current_user()->display_name;?> </h4>
                      <p class="text-secondary mb-1"><?php echo get_metadata( 'user', $current_user->ID,'education',true )?></p>
                      <p class="text-muted font-size-sm"><?php echo get_metadata( 'user', $current_user->ID,'user_specialty',true )?></p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-8">
              <div class="card mb-3" >
                <div class="card-body" >
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">نام</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                    <?php echo the_author_meta( 'first_name', $current_user->ID );  ?>
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">نام خانوادگی</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                    <?php echo the_author_meta( 'last_name', $current_user->ID );  ?>
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">نام کاربری</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                    <?php echo wp_get_current_user()->user_login ;  ?>
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">وبسایت</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                    <?php echo the_author_meta( 'user_url', $current_user->ID );  ?>
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">ایمیل</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                    <?php echo the_author_meta( 'user_email', $current_user->ID );  ?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
    </div>

<?php
 //var_dump(wp_get_current_user());
 //var_dump( get_the_author_meta('description', $post->post_author));
 //echo the_author_meta( 'user_activation_key', $current_user->ID );
?>


    <?php
    $id = $current_user->ID;
    global $paged;
    $curpage = $paged ? $paged : 1;
    $args = array(
    'orderby' => 'date',
    'posts_per_page' => -4,
    'author' => $id,
    'paged' => $paged
    );
    $query = new wp_query($args);
    ?>

<div class="container blog-page">
    <div class="row">
        <div class="panel panel-default widget">
            <div class="panel-heading">
                <span class="glyphicon glyphicon-comment"></span>
                <h3 class="panel-title">
                   :نوشته های شما</h3>
                <span class="label label-info"> <?php echo $query->found_posts; ?></span>
            </div>
            <div class="panel-body">






                <ul class="list-group">
                <?php if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>
                    <li class="list-group-item">
                        <div class="row">
                            <div class="col-xs-2 col-md-1">
                                <img src="<?php echo get_the_post_thumbnail_url($post->ID, 'full'); ?>" class="img-circle img-responsive" alt="" /></div>
                            <div class="col-xs-10 col-md-11">
                                <div>
                                    <a href="<?php echo the_permalink(); ?>">
                                    <?php the_title(); ?></a>
                                    <div class="mic-info">
                                       <span><?php echo get_the_date() ?></span> 
                                    </div>
                                </div>

                                <div  id="edit-user-post">
                                    <a href="<?php echo get_edit_post_link()?>" class="btn btn-success btn-xs" title="Approved">
                                    <i class="fa fa-pencil" style="font-size:14px"></i>
                                    </a>
                                    <a id="edit-" href="<?php echo get_delete_post_link()?>" class="btn btn-danger btn-xs" title="Delete">
                                    <i class="fa fa-trash" style="font-size:14px"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </li>
                    <?php endwhile;?>
                    </ul>
                    <?php
                    echo '
                    <div id="wp_pagination">
                        <a class="first page button" href="'.get_pagenum_link(1).'">&laquo;</a>
                        <a class="previous page button" href="'.get_pagenum_link(($curpage-1 > 0 ? $curpage-1 : 1)).'">&lsaquo;</a>';
                        for($i=1;$i<=$query->max_num_pages;$i++)
                            echo '<a class="'.($i == $curpage ? 'active ' : '').'page button" href="'.get_pagenum_link($i).'">'.$i.'</a>';
                        echo '
                        <a class="next page button" href="'.get_pagenum_link(($curpage+1 <= $query->max_num_pages ? $curpage+1 : $query->max_num_pages)).'">&rsaquo;</a>
                        <a class="last page button" href="'.get_pagenum_link($query->max_num_pages).'">&raquo;</a>
                    </div>
                    ';
                    endif;
                    ?>

                
            </div>
        </div>
    </div>
</div>
         

            <?php
            //var_dump( get_query_var('paged'));
            //var_dump( $current_user  );
            //var_dump(get_metadata( 'user', $current_user->ID ));
            //var_dump(get_field( "user_image2", $current_user->ID ));
           //$val=get_field('user_image', $current_user->ID);
           //var_dump($val);
           //var_dump(get_post_field('user_image',$current_user->ID));
           //var_dump(get_field('user_image', 'user_'.$current_user->ID));
           //var_dump(get_metadata( 'user', $current_user->ID));
           //echo get_metadata( 'user', $current_user->ID,'user_specialty',true );
           //var_dump(wp_get_current_user());
            ?>
        </div>
    </div>




<?php get_footer(); ?>