<?php get_header(); ?>
<?php 
    $args = array(
      'orderby' => 'date',
      'posts_per_page' => -3,
      'post_type' => 'post'
      );
      $myquery = new wp_query($args);

?>


<div class="container blog-page">

        <div id="myCarousel" class="carousel slide" data-bs-ride="carousel">
        <div class="carousel-indicators">
          <button type="button" data-bs-target="#myCarousel" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
          <button type="button" data-bs-target="#myCarousel" data-bs-slide-to="1" aria-label="Slide 2"></button>
          <button type="button" data-bs-target="#myCarousel" data-bs-slide-to="2" aria-label="Slide 3"></button>
        </div>
        <div class="carousel-inner">
        <?php if ( $myquery -> have_posts() ) : while ( $myquery -> have_posts() ) : $myquery -> the_post(); $count++ ?>
      <div class="carousel-item  <?php if( $count==1){echo 'active' ;} ?> ">
        <img src="<?php echo get_the_post_thumbnail_url($post->ID, 'full'); ?>" class="bd-placeholder-img" width="100%" height="300px" xmlns="http://www.w3.org/2000/svg" aria-hidden="true" preserveAspectRatio="xMidYMid slice" focusable="false"><rect width="100%" height="100%" fill="#777">


        <div class="container">
          <div class="carousel-caption text-start">
            <h1><?php the_title(); ?></h1>
            <p><?php echo wp_trim_words(get_the_excerpt(), 10); ?></p>
            <p><a class="btn btn-lg btn-primary" href="<?php the_permalink(); ?>">ادامه ... </a></p>
          </div>
        </div>
      </div>
      <?php endwhile;endif; ?>
    </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#myCarousel" data-bs-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#myCarousel" data-bs-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Next</span>
        </button>
</div>


        <div class="row clearfix">
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
          <div id="index" class="col-lg-4 col-md-12 my_post_id-<?php the_ID(); ?> ">
                <div class="card single_post">
                    <div class="header">
                    <h2> روتیتر <?php the_title(); ?></h2>

                    </div>
                    <div class="body">
                        <h3 class="m-t-0 m-b-5"><a href="<?php echo the_permalink(); the_permalink(); ?>"><h2><?php the_title(); ?></h2></a></h3>
                        <ul class="meta">
                            <li><a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) );?>" ><i class="zmdi zmdi-account col-blue"></i><?php the_author(); ?></a></li>
                            <li><a href="javascript:void(0);"><i class="zmdi zmdi-label col-amber"></i>	<?php the_tags( 'Tags: ', ', ', '<br />' ); ?></a></li>
                            <li><a href="javascript:void(0);"><i class="zmdi zmdi-comment-text col-blue"></i>Comments: 3</a></li>
                        </ul>
                    </div>
                    <div class="body">
                        <div class="img-post m-b-15">
                            <img src="<?php echo get_the_post_thumbnail_url($post->ID, 'full'); ?>" alt="Awesome Image">
                            <div class="social_share">
                                <button class="btn btn-primary btn-icon btn-icon-mini btn-round"><i class="zmdi zmdi-facebook"></i></button>
                                <button class="btn btn-primary btn-icon btn-icon-mini btn-round"><i class="zmdi zmdi-twitter"></i></button>
                                <button class="btn btn-primary btn-icon btn-icon-mini btn-round"><i class="zmdi zmdi-instagram"></i></button>
                            </div>
                        </div>
                        <p><?php echo wp_trim_words(get_the_excerpt(), 50); ?></p>
                        <a href="<?php the_permalink(); ?>" title="read more" class="btn btn-round btn-info">Read More</a>
                    </div>
                </div>
            </div>

            <?php
          //var_dump($post);
          
          endwhile;endif; ?>
            <div class="pagination-1" >
            <?php wpbeginner_numeric_posts_nav(); ?>
            </div>
                <nav class="pagination-2" aria-label="...">
                <ul class="pagination">
                    <li class="page-item">
                    <a class="page-link" href="<?php echo my_get_previous_posts_link(); ?>" >Previous</a>
                    </li>
                    <li class="page-item">
                    <a class="page-link" href="<?php echo my_get_next_posts_link(); ?>">Next</a>
                    </li>
                </ul>
                </nav>
        </div>
    </div>    

    <?php get_footer(); ?>





