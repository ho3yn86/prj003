

<?php /* Template Name: قالب شماره 1 */ ?>
<?php get_header(); ?>
<?php 
    $args = array(
      'orderby' => 'date',
      'posts_per_page' => -3,
      'post_type' => 'post'
      );
      $myquery = new wp_query($args);

?>


<div class="container blog-page">



        <div class="row clearfix">
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
          <div id="index" class="col-lg-4 col-md-12 my_post_id-<?php the_ID(); ?> ">
                <div class="card single_post">
                    <div class="header">
                    <h2> روتیتر <?php the_title(); ?></h2>

                    </div>
                    <div class="body">
                        <h3 class="m-t-0 m-b-5"><a href="<?php echo the_permalink(); the_permalink(); ?>"><h2><?php the_title(); ?></h2></a></h3>
                        <ul class="meta">
                            <li><a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) );?>" ><i class="zmdi zmdi-account col-blue"></i><?php the_author(); ?></a></li>
                            <li><a href="javascript:void(0);"><i class="zmdi zmdi-label col-amber"></i>	<?php the_tags( 'Tags: ', ', ', '<br />' ); ?></a></li>
                            <li><a href="javascript:void(0);"><i class="zmdi zmdi-comment-text col-blue"></i>Comments: 3</a></li>
                        </ul>
                    </div>
                    <div class="body">
                        <div class="img-post m-b-15">
                            <img src="<?php echo get_the_post_thumbnail_url($post->ID, 'full'); ?>" alt="Awesome Image">
                            <div class="social_share">
                                <button class="btn btn-primary btn-icon btn-icon-mini btn-round"><i class="zmdi zmdi-facebook"></i></button>
                                <button class="btn btn-primary btn-icon btn-icon-mini btn-round"><i class="zmdi zmdi-twitter"></i></button>
                                <button class="btn btn-primary btn-icon btn-icon-mini btn-round"><i class="zmdi zmdi-instagram"></i></button>
                            </div>
                        </div>
                        <p><?php echo wp_trim_words(get_the_excerpt(), 50); ?></p>
                        <a href="<?php the_permalink(); ?>" title="read more" class="btn btn-round btn-info">Read More</a>
                    </div>
                </div>
            </div>

            <?php
          //var_dump($post);
          
          endwhile;endif; ?>
            <div class="pagination-1" >
            <?php wpbeginner_numeric_posts_nav(); ?>
            </div>
                <nav class="pagination-2" aria-label="...">
                <ul class="pagination">
                    <li class="page-item">
                    <a class="page-link" href="<?php echo my_get_previous_posts_link(); ?>" >Previous</a>
                    </li>
                    <li class="page-item">
                    <a class="page-link" href="<?php echo my_get_next_posts_link(); ?>">Next</a>
                    </li>
                </ul>
                </nav>
        </div>
    </div>    

    <?php get_footer(); ?>





